package de.dlr.gitlab.fame;

import static com.google.testing.compile.CompilationSubject.assertThat;
import static com.google.testing.compile.Compiler.javac;
import com.google.testing.compile.Compiler;
import javax.tools.JavaFileObject;
import org.junit.Before;
import org.junit.Test;
import com.google.testing.compile.Compilation;

/**
 * Tests for {@link InputProcessor} using com.google.testing.compile
 * 
 * @see https://javadoc.io/doc/com.google.testing.compile/compile-testing/latest/index.html
 * 
 * @author Christoph Schimeczek
 */

public final class InputProcessorTest {
	private Compiler compiler;

	@Before
	public void setUp() {
		compiler = javac().withProcessors(new InputProcessor());
	}

	@Test
	public void testFailOnWrongType() {
		JavaFileObject sourceFile = SourceFileBuilder.newBuilder("TestClass").ext("Agent")
				.impo("de.dlr.gitlab.fame.agent.input.Input").impo("de.dlr.gitlab.fame.agent.Agent")
				.line("@Input").line("public static final int number = 5;").build();
		Compilation compilation = compiler.compile(sourceFile);
		assertThat(compilation).failed();
		assertThat(compilation).hadErrorContaining(InputProcessor.ERR_NOT_PARAM_TREE);
	}

	@Test
	public void testFailOnNotAgent() {
		JavaFileObject sourceFile = SourceFileBuilder.newBuilder("TestClass")
				.impo("de.dlr.gitlab.fame.agent.input.Input").impo("de.dlr.gitlab.fame.agent.input.ParameterTree")
				.line("@Input").line("public static final ParameterTree param = null;").build();
		Compilation compilation = compiler.compile(sourceFile);
		assertThat(compilation).succeeded();
		assertThat(compilation).hadWarningContaining(InputProcessor.WARN_NOT_AGENT);
	}

	@Test
	public void testFailNotFinal() {
		JavaFileObject sourceFile = SourceFileBuilder.newBuilder("TestClass").ext("Agent")
				.impo("de.dlr.gitlab.fame.agent.input.Input").impo("de.dlr.gitlab.fame.agent.input.ParameterTree")
				.impo("de.dlr.gitlab.fame.agent.Agent")
				.line("@Input").line("public static ParameterTree param = null;").build();
		Compilation compilation = compiler.compile(sourceFile);
		assertThat(compilation).failed();
		assertThat(compilation).hadErrorContaining(InputProcessor.ERR_NOT_STATIC_FINAL);				
	}
	
	@Test
	public void testFailNotStatic() {
		JavaFileObject sourceFile = SourceFileBuilder.newBuilder("TestClass").ext("Agent")
				.impo("de.dlr.gitlab.fame.agent.input.Input").impo("de.dlr.gitlab.fame.agent.input.ParameterTree")
				.impo("de.dlr.gitlab.fame.agent.Agent")
				.line("@Input").line("public final ParameterTree param = null;").build();
		Compilation compilation = compiler.compile(sourceFile);
		assertThat(compilation).failed();
		assertThat(compilation).hadErrorContaining(InputProcessor.ERR_NOT_STATIC_FINAL);				
	}	
}
